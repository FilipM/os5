#include <utility>

#include <iostream>
#include <random>
#include <mutex>
#include <thread>
#include <chrono>
#include <algorithm>
#include <cstdlib>

enum class Jela : int{cokolada = 1, cevapi = 2, janjetina = 3, strukle = 4, grah = 5, keksi = 6, kelj = 7, jabuka = 8};
std::vector<Jela> Stol;
std::mutex brava;

template<typename Tip>
Tip Rand(int broj1, int broj2);

int factorial(int n);

struct Osoba{
    std::string ime;
    int pojeo = 0;
    int odbio = 0;
    int prazanStol = 0;

    explicit Osoba(std::string ime)
            :ime(std::move(ime)) { }
};

Osoba Kuharica{"Kuharica"}, Tin{"Tin"}, Davor{"Davor"}, Ivica{"Ivica"}, Ivan{"Ivan"};

int jedi(Osoba &osoba, std::vector<Jela> &jela){
    brava.lock();
    if(osoba.ime == "Kuharica"){
        if(Stol.empty()){
            Stol.push_back(jela.back());
            jela.pop_back();
        }
        else{
            brava.unlock();
            return -1;
        }
    }else{
        bool pojeo = false;
        std::cout << osoba.ime << " sada jede." << std::endl;
        if(Stol.empty()){
            std::cout << osoba.ime << " je naisao na prazan stol" << std::endl;
            osoba.prazanStol++;
        }else {
            for (auto& item : jela) {
                if (item==Stol.back()) {
                    std::cout << osoba.ime << " je pojeo hranu na stolu" << std::endl;
                    Stol.pop_back();
                    osoba.pojeo++;
                    pojeo = true;
                    break;
                }
            }
            if (!pojeo) {
                std::cout << osoba.ime << " nije pojeo hranu na stolu" << std::endl;
                osoba.odbio++;
            }
        }
    }

    brava.unlock();
    return 0;
}

void kuharica(int br_jela){
    int br = br_jela, status;
    std::vector<Jela> jela;
    Jela jelo;
    while(br >= 0){
        std::cout << "Kuharica kuha jelo" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(1, 2)));
        jelo = Jela{Rand<int>(1, 8)};
        jela.push_back(jelo);
        std::cout << "Kuharica stavlja jelo na stol" << std::endl;
        while(true){
            if(Stol.empty()){
                Stol.push_back(jelo);
                break;
            }
            //std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        /*do{
            status = jedi(Kuharica, jela);
        }while(status == -1);*/
        std::cout << "Kuharica se odmara" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(1, 2)));
        br--;
    }

}

void tin(){
        std::vector<Jela> jelaKojaJede{Jela::cokolada, Jela::strukle, Jela::grah, Jela::keksi,
                                       Jela::kelj, Jela::jabuka};
    while(true) {
        std::cout << "Tin sada spava" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(6, 10)));
        std::cout << "Tin sada programira" << std::endl;
        factorial(Rand<int>(1, 10));
        //jede
        jedi(Tin, jelaKojaJede);
        std::cout << "Tin sada ceka popravak alfe :(" << std::endl;
        auto popravak = Rand<int>(1, 100);
        if (popravak<=25)
            std::this_thread::sleep_for(std::chrono::seconds(4));
        else if (popravak>25 && popravak<=35)
            std::this_thread::sleep_for(std::chrono::seconds(2));
        else
            std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(2, 4)));
    }
}

void davor(){
        std::vector<Jela> jelaKojaJede{Jela::cevapi, Jela::janjetina,
                                       Jela::grah, Jela::kelj,
                                       Jela::strukle, Jela::jabuka};
    while(true) {
        std::cout << "Davor sada spava" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(5, 10)));
        std::cout << "Davor sada programira" << std::endl;
        factorial(Rand<int>(1, 10));
        //jede
        jedi(Davor, jelaKojaJede);
        std::cout << "Davor sada gleda televiziju" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(2, 4)));
    }
}
void ivica(){
        std::vector<Jela> jelaKojaJede{Jela::cokolada, Jela::cevapi,
                                       Jela::janjetina, Jela::strukle,
                                       Jela::keksi, Jela::jabuka};
    while(true) {
        std::cout << "Ivica sada spava" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(5, 10)));
        std::cout << "Ivica sada igra tenis" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(2, 4)));
        // jede
        jedi(Ivica, jelaKojaJede);
        std::cout << "Ivica sada programira" << std::endl;
        factorial(Rand<int>(1, 10));
    }
}
void ivan(){
    std::vector<Jela> jelaKojaJede{Jela::cokolada, Jela::cevapi,
                                                           Jela::janjetina, Jela::grah,
                                                           Jela::keksi, Jela::kelj,
                                                           Jela::jabuka};
    while(true) {
        std::cout << "Ivan sada spava" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(5, 10)));
        std::cout << "Ivan sada slusa klavir" << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(Rand<int>(2, 4)));
        //jede
        jedi(Ivan, jelaKojaJede);
        std::cout << "Ivan sada programira" << std::endl;
        factorial(Rand<int>(1, 10));
    }
}

int main(int argc, char ** argv){
    if(argc > 2){
        std::cout << "Unjeli ste previše parametara!" << std::endl;
        return 1;
    }
    if(argc < 2){
        std::cout << "Unjeli ste premalo parametara!" << std::endl;
        return 1;
    }

    std::thread kuharicaThread{kuharica, std::stoi(std::string(argv[1]))};
    std::thread TinThread{tin};
    std::thread DavorThread{davor};
    std::thread IvicaThread{ivica};
    std::thread IvanThread{ivan};

    kuharicaThread.join();

    TinThread.detach();
    DavorThread.detach();
    IvicaThread.detach();
    IvanThread.detach();

    std::cout << std::endl;
    std::cout << std::endl;
    std::vector<Osoba> osobe{Tin, Davor, Ivica, Ivan};
    while(true) {
        if(Stol.empty()){
            for(auto &osoba : osobe){
                std::cout << osoba.ime << " je pojeo: " << osoba.pojeo << " odbio: "
                          << osoba.odbio << " naisao na prazan stol: " << osoba.prazanStol
                          << std::endl;
            }
            break;
        }
    }

    return 0;
}

template<typename Tip>
Tip Rand(int broj1, int broj2){
    std::random_device rd;
    std::mt19937 mt(rd());
    if constexpr (std::is_same_v<Tip, int> || std::is_same_v<Tip, long>) {
        std::uniform_int_distribution<Tip> distribucija_tip(broj1, broj2);
        return distribucija_tip(mt);
    } else if constexpr (std::is_same_v<Tip, double> || std::is_same_v<Tip, float>) {
        std::uniform_real_distribution<Tip> distribucija_tip(broj1, broj2);
        return distribucija_tip(mt);
    }
    return -1;
}

int factorial(int n){
    if(n == 0)
        return 1;
    return n * factorial(n - 1);
}
